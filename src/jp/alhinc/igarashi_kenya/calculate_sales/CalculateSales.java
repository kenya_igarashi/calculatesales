package jp.alhinc.igarashi_kenya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店定義ファイルを保持するリスト
		Map<String, String> branchName = new HashMap<>();
		//商品定義ファイルを保持するリスト
		Map<String, String> commodityName = new HashMap<>();
		//支店ごとの売り上げ情報を保持するリスト
		Map<String, Long> branchSale = new HashMap<>();
		//商品ごとの売り上げ情報を保持するリスト
		Map<String, Long> commoditySale = new HashMap<>();
		BufferedReader br = null;

		//支店別集計ファイル入力とエラーチェック
		if (!inputFile(args[0], "branch.lst", "支店", "^\\d{3}$", branchName, branchSale)) {
			return;
		}
		//商品別集計ファイル入力とエラーチェック
		if (!inputFile(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]+$", commodityName, commoditySale)) {
			return;
		}

		//ファイル名を全取得
		File[] sales = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<>();
		int i;
		for (i = 0; i < sales.length; i++) {
			//matchesを適用するためにString型に
			String fileName = sales[i].getName();
			//売り上げファイルのみを抽出
			//ファイルの名前を取り出す
			//それに対してmatchesを適用
			if (fileName.matches("^\\d{8}.rcd$") && sales[i].isFile()) {
				//売り上げファイルのみをリストrcdFilesに追加
				rcdFiles.add(sales[i]);
			}
		}
		//MacOS対応用処置
		Collections.sort(rcdFiles);
		//売り上げファイルの連番を確認
		for (int j = 0; j < rcdFiles.size() - 1; j++) {
			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(j + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println("売り上げファイル名が連番になっていません");
				return;
			}
		}
		//rcdファイルの数だけ繰り返す
		for (i = 0; i < rcdFiles.size(); i++) {
			File rcdfile = rcdFiles.get(i);
			try {
				FileReader saleFr = new FileReader(rcdfile);
				br = new BufferedReader(saleFr);
				String saleLine;
				//rcdFilesをFile型に
				ArrayList<String> saleArrayList = new ArrayList<>();
				while ((saleLine = br.readLine()) != null) {
					//行数だけ売り上げ情報をArrayに保持
					saleArrayList.add(saleLine);
				}
				//rcdFilesの行数チェック
				if (saleArrayList.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				//ArryaListの売り上げ参照をString型の変数に代入
				String saleKey = saleArrayList.get(0);
				String saleCommodityNumber = saleArrayList.get(1);
				String saleList = saleArrayList.get(2);
				//String型の変数をLong型に変換
				long conversionSaleList = Long.parseLong(saleList);
				//売上金額が数値か確認
				if (!saleList.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売り上げファイルの支店コードが支店定義ファイルに無いエラー
				if (!branchName.containsKey(saleKey)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				//売り上げファイルの商品コードが商品定義ファイルに無いエラー
				if (!commodityName.containsKey(saleCommodityNumber)) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}
				//支店売り上げ反映後に10桁を超えるかチェック
				if (branchSale.get(saleKey) + conversionSaleList >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//商品売り上げ反映後に10桁を超えるかチェック
				if (commoditySale.get(saleCommodityNumber) + conversionSaleList >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//支店ごとの売り上げのマップに反映
				branchSale.put(saleKey, branchSale.get(saleKey) + conversionSaleList);
				//商品ごとの売り上げのマップに反映
				commoditySale.put(saleCommodityNumber, commoditySale.get(saleCommodityNumber) + conversionSaleList);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					if (br != null) {
						br.close();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//支店別集計ファイル出力とエラーチェック
		if (!outputFile(args[0], "branch.out", branchName, branchSale)) {
			return;
		}
		//商品別集計ファイル出力とエラーチェック
		if (!outputFile(args[0], "commodity.out", commodityName, commoditySale)) {
			return;
		}
	}

	//集計ファイル入力メソッド
	private static boolean inputFile(String path, String fileName, String category, String regex,
			Map<String, String> name, Map<String, Long> sale) {
		BufferedReader br = null;
		//定義ファイルの形式確認
		File readingFile = new File(path, fileName);
		//定義ファイルの存在確認
		if (!readingFile.exists()) {
			System.out.println(category + "定義ファイルが存在しません");
			return false;
		}
		try {
			//定義ファイルを読み込む
			FileReader fr = new FileReader(readingFile);
			br = new BufferedReader(fr);
			String nameLine;
			while ((nameLine = br.readLine()) != null) {
				//,で分ける
				String[] readingList = nameLine.split(",");
				//読み込んだファイルがどちらか識別しチェック
				if ((readingList.length != 2) || (!readingList[0].matches(regex))) {
					System.out.println(category + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//読み込んだ定義を保持
				name.put(readingList[0], readingList[1]);
				//後で加算するためにここで売り上げに0の値を与える
				sale.put(readingList[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

	//集計ファイル出力メソッド
	private static boolean outputFile(String path, String fileName, Map<String, String> name,
			Map<String, Long> sale) {
		BufferedWriter bw = null;
		//書き込むファイルを指定
		File writingFile = new File(path, fileName);
		try {
			FileWriter fw = new FileWriter(writingFile);
			bw = new BufferedWriter(fw);
			//読み込んだキーの数だけキーの記入、
			for (String writingKey : name.keySet()) {
				//マップから読み込んだキーを格納するリスト

				bw.write(writingKey + "," + name.get(writingKey) + ","
						+ sale.get(writingKey));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (bw != null) {
					bw.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

}

//メモ
//プッシュ確認用に時間記入
//15:05
//修正内容
//インデントを修正
//質問内容
